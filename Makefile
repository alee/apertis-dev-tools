all clean install:
	$(MAKE) -C data $@
	$(MAKE) -C doc $@
	$(MAKE) -C tests $@
	$(MAKE) -C tools $@

check: all
	$(MAKE) -C tests check

dist-check: check
