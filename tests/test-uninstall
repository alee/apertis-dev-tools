#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import os
import shutil
import tempfile

from test_util import should_succeed, should_fail, split_elements
from test_util import BASE_TAG, BASE_INSTALL, LATEST_SYSROOTS

# Utility functions
def check_list(path, sysroots):
    expected_tags = {BASE_TAG.format(*sysroot) for sysroot in sysroots}
    result = should_succeed('sysroot', '--path', path, 'list')
    return expected_tags == set(split_elements(result['InstalledSysroots']))


with tempfile.TemporaryDirectory() as tmpdir:
    # Copy install directory
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    path = os.path.join(tmpdir, 'install')
    shutil.copytree(BASE_INSTALL, path)

    if not check_list(path, LATEST_SYSROOTS):
        exit(1)

    # Uninstall all sysroots
    remaining_sysroots = set(LATEST_SYSROOTS)
    for sysroot in LATEST_SYSROOTS:
        remaining_sysroots.remove(sysroot)
        params = ['--distro', sysroot[0], '--release', sysroot[1], '--arch', sysroot[2]]
        should_succeed('sysroot', '--path', path, 'uninstall', *params,
                       check=lambda x: check_list(path, remaining_sysroots))
        tag = '{}-{}-{}'.format(*sysroot)
        should_succeed('info', '--path', BASE_INSTALL, '--sysroot', tag)
        should_fail('info', '--path', path, '--sysroot', tag)

    if not check_list(path, []):
        exit(1)

    # Try to uninstall non-existing sysroot
    params = ['--distro', 'eraroj', '--release', '16.09', '--arch', 'armhf']
    should_fail('sysroot', '--path', path, 'uninstall', *params)
