#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import os
import shutil
import tempfile
import subprocess

from test_util import should_succeed, should_fail, split_elements
from test_util import BASE_TAG, BASE_INSTALL, BASE_MOCK, LATEST_SYSROOTS

# Utility functions
def check_list(path, sysroots):
    expected_tags = {BASE_TAG.format(*sysroot) for sysroot in sysroots}
    result = should_succeed('sysroot', '--path', path, 'list')
    return expected_tags == set(split_elements(result['InstalledSysroots']))


with tempfile.TemporaryDirectory() as tmpdir:
    # Copy install directory
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    path = os.path.join(tmpdir, 'install')
    shutil.copytree(BASE_INSTALL, path)
    mock = os.path.join(tmpdir, 'mock')
    shutil.copytree(BASE_MOCK, mock)

    if not check_list(path, LATEST_SYSROOTS):
        exit(1)

    os.chdir(mock);

    for sysroot in LATEST_SYSROOTS:
        tag = '{}-{}-{}'.format(*sysroot)
        build = os.path.join(mock, tag)
        should_succeed('configure', '--path', path, '--sysroot', tag,
                       '--build-dir', build)

