#!/bin/sh

SOURCE=`dirname $0`
TARGET=`pwd`

echo "SOURCE $SOURCE"
echo "TARGET $TARGET"
echo "Generating configure $*"
cp $SOURCE/configure.in $TARGET/configure
